$(function () {
	"use strict";

	$('.filter-sidebar__button').click(function() {
		$(this).toggleClass('sort-current');
		$(this).next( ".sidebar-dropdown" ).toggleClass('sort-open');
	});

	$('#slider-price').slider({
		max: 700,
		min: 0,
		range: true,
		values: [52, 400],
		slide: function ( event, ui) {
			$('input[name="minPrice"]').val( '$' + ui.values[0]);
			$('input[name="maxPrice"]').val( '$' + ui.values[1]);
		}
	});
	$('input[name="minPrice"]').val('$' + $('#slider-price').slider('values', 0));
	$('input[name="maxPrice"]').val('$' + $('#slider-price').slider('values', 1));

	$('.sort-row__btn').click(function() {
		$(this).next( ".sort-row-dropdown__menu" ).toggleClass('open');
	});

	$('#cart-country-btn').click(function() {
		$(this).next( ".sort-row-dropdown__menu--cart" ).toggleClass('put');
	});
});

$(document).ready(function(){
      $('.bxslider').bxSlider({
      	pager: false
      });
    });

var accordion = function(){
  var data = $('.accordion').attr('data-accordion')
  
  $('.accordion-header').on('click', function(){
    $(this).next('.accordion-body').not(':animated').slideToggle()
  })
}

accordion();